'''
@file lab7_TaskMotor.py
@brief Task that generates reference profiles from a csv and actuates the motor accordingly
@details This file operates on the Nucleo L476 and interacts with lab7_UserInt.py 
on the PC to receive user input Kp value, generate Reference Velocity and Position 
profiles from lab7_reference.csv, truncate the reference profiles to a usable size, 
actuate the motor according to these inputs, record and format actual 
Motor speed and position data data, and send this data back to lab7_UserInt.py.
@author Jacob Winkler 
'''

import pyb
import array
from pyb import UART
import utime 
from encoder import Encoder
from closedloop import closedloop
from MotorDriver import MotorDriver



class MotorTask:
    '''
    @brief Motor task for ME305 lab 7 that receives the user input Kp from the PC, actuates 
    the motor, records motor speed and position data, and sends this data back 
    to the PC
    '''
    ## Initializing 
    S0_INIT                 = 0
    
    ## Controlling Data Collection
    S1_CONTROLLING          = 1
    
    ## Sends motor data arrays to PC to be plotted 
    S2_SEND_DATA            = 2
    
    ## Ends FSM loop and exits the task; note that this task only runs once 
    S3_FINISH               = 3
    
    def __init__(self, interval):
        '''
        @brief Creates a motor task object
        @param interval Defines the interval at which the task will loop.
        '''
        
        ## Initiate Serial Port Communication
        self.ser = UART(2)
        
        ## stores copies of encoder, motordriver, and closedloop classes
        self.Encoder = Encoder('C6', 'C7', 8) 
        self.MotorDriver = MotorDriver('A15', 'B4', 'B5', 3)
        self.closedloop = closedloop
        
        ## stores class copy of array import
        self.array = array
        
        ## updates initial encoder position
        self.Encoder.update()
        
        ## The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enables motor to spin
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        
        ## Preallocate Data Arrays
        self.velocity_act = self.array.array('f', [0])
        self.position_act = self.array.array('f', [0])
        self.time = self.array.array('f', [0])
        self.trunc_time = self.array.array('f', [0])
        self.trunc_vel = self.array.array('f', [0])
        self.trunc_pos = self.array.array('f', [0])
        self.J_array = self.array.array('f', [0])
    
        ## Initial State
        self.state = self.S0_INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            self.Encoder.update()
            if (self.state == self.S0_INIT):
                if self.ser.any():
                    
                    self.Kp = float(self.ser.readline())
                    
                    ## Initiate closedloop controller class with the Kp value received from the PC and PWM saturation limits of -100 and 100
                    self.ClosedLoopCont = closedloop(self.Kp, -100, 100)
                    
                    ## Opens and reads csv file with refence velocity and position profiles, along with their corresponding times 
                    ref_data = open('lab7_reference.csv')
                    while True:
                        line = ref_data.readline()
                        if line == '':
                            break
                        else:
                            (t,v,x) = line.strip().split(',');
                            #truncates data according to our 20 millisecond interval
                            if float(t)%0.02 < 5E-4:
                                self.trunc_time.append(float(t))
                                self.trunc_vel.append(float(v))
                                self.trunc_pos.append(float(x))
                                
                        line = None
                    
                    self.runs = 0
                    self.transitionTo(self.S1_CONTROLLING)
                    
            if (self.state == self.S1_CONTROLLING):
                
                if len(self.time) < len(self.trunc_time):
                    ## Read Encoder delta to calculate actual RPM
                    PPR = 4000 #Pulses per rev
                    revs = self.Encoder.get_delta()/PPR #converts encoder position delta to number of revolutions in motor shaft 
                    int_sec = self.interval*1E-6 #converts interval value to seconds
                    int_min = int_sec/60 #converts interval value to minutes
                    actual_velocity = revs/int_min # calculates motor speed in RPM
                    
                    ## Read Encoder position and convert to degrees
                    PPD = PPR/360 #Pulses per degree
                    actual_position = self.Encoder.get_position()/PPD #converts encoder position to motor shaft position in degrees
                    
                    ## Write relevant data to preallocated data arrays
                    self.velocity_act.append(actual_velocity)
                    self.position_act.append(actual_position)
                    self.time.append((self.curr_time-self.start_time)/1E6)
                    
                    ## Calculate Control Loop Gain
                    L = self.ClosedLoopCont.update(self.trunc_vel[self.runs], actual_velocity)
                    actual_velocity = None
                    actual_position = None
                    ## Change motor duty cycle based on Control Loop Output
                    self.MotorDriver.set_duty(L)
                    
                    ## Calculate Individual J values for each recorded velocity and position values using a Reimann Sum and recording it to J_array
                    vel_diff = self.trunc_vel[self.runs] - self.velocity_act[self.runs]
                    pos_diff = self.trunc_pos[self.runs] - self.position_act[self.runs]
                    J = vel_diff**2 + pos_diff**2
                    self.J_array.append(J)
                    
                    ## increment run count
                    self.runs += 1
                    
                else:
                    self.transitionTo(self.S2_SEND_DATA)
                    
            if (self.state == self.S2_SEND_DATA):
                
                ## set duty cycle to zero so that Motor stops
                self.MotorDriver.set_duty(0)
                    
                ## Summing all of the individual J values and dividing by the total number of iterations to complete the Reimann sum
                J = sum(self.J_array)/len(self.J_array)
                
                ## Writing Data arrays to serial bus
                self.ser.write(str(J)+'\n')
                self.ser.write(str(self.time)+'\n')
                self.ser.write(str(self.velocity_act)+'\n')
                self.ser.write(str(self.position_act)+'\n')
                self.ser.write(str(self.trunc_vel)+'\n')
                self.ser.write(str(self.trunc_pos)+'\n')
                
                self.transitionTo(self.S3_FINISH)
                
            if (self.state == self.S3_FINISH):
                pass
            
            else:
                pass

            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState
            
        