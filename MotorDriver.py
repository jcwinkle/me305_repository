''' 
@file MotorDriver.py
@brief Doc defining generic Motor Driver class
@author Jacob Winkler (adapted from code by Charlie Refvem)
'''

import pyb
class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 PCB board. 
    '''
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin. Input must be a string!!!
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1. Input must be a string!!!
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2. Input must be a string!!!
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        
        '''
        
        self.pin_nSLEEP = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        pin1 = pyb.Pin(IN1_pin)
        pin2 = pyb.Pin(IN2_pin)
        tim = pyb.Timer(timer, freq=20000)
        self.chan1 = tim.channel(1, pyb.Timer.PWM, pin=pin1)
        self.chan2 = tim.channel(2, pyb.Timer.PWM, pin=pin2)
        self.chan1.pulse_width_percent(0)
        self.chan2.pulse_width_percent(0)
        self.pin_nSLEEP.low()
        print ('Creating the Motor driver')

    def enable (self):
        ''' 
        @brief Sets pin_nSLEEP to high, allowing motor to be actuated
        '''
        self.pin_nSLEEP.high()
        print ('Motor is enabled')

    def disable (self):
        ''' 
        @brief Sets pin_nSLEEP to high, disabling the motor from being actuated
        '''
        self.pin_nSLEEP.low()
        print ('Motor is disabled')

    def set_duty (self, duty):
        ''' 
        @brief This method sets the duty cycle to be sent to the motor to the given level. 
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        
        '''
        if duty >= 0:
            self.chan1.pulse_width_percent(duty)
            self.chan2.pulse_width_percent(0)
        elif duty < 0:
            duty = abs(duty)
            self.chan1.pulse_width_percent(0)
            self.chan2.pulse_width_percent(duty)
        

if __name__ == '__main__':
    ## Test Code
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = 'A15';
    pin_IN1 = 'B4';
    pin_IN2 = 'B5';

    # Create the timer object used for PWM generation
    tim = 3;

    # Create a motor object passing in the pins and timer
    mot = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    mot.enable()

    # User sets the duty cycle 5 different times, then the motor turns off
    n = 1
    while n <= 5:
        In = input('Please input desired motor speed, Your Grace (-100 to 100): ')
        mot.set_duty(int(In))
        n += 1
    
    mot.set_duty(0)
    mot.disable()