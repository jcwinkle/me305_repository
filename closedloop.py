'''
@file closedloop.py
@brief Script defining a class for a closed loop p-controller. 
@details When called regularly, 
this computes and returns the actuation value based on the measured and reference 
values. The class can also return the current Kp value as well as set the Kp value.
@author Jacob Winkler 
'''

class closedloop:

    def __init__(self, Kp, min_val, max_val):
        '''
        @brief Creates a closedloop object, initites feedback system
        @param Kp predefined proportionality constant 
        @param min_val lower saturation limit for the system
        @param max_val upper saturation limit for the system
        '''
        self.Kp = Kp
        self.max_val = max_val
        self.min_val = min_val
        
    def update(self, ref_val, meas_val):
        '''
        @brief Given a single input reference value and a measured value, calculates actuation value using the predefined Kp within the predefined saturation limits.
        @param ref_val input reference (desired) velocity value
        @param meas_val input actual (measured) velocity value
        
        '''
        L = self.Kp*(ref_val - meas_val)
        if L < self.min_val:
            L = self.min_val
        elif L > self.max_val:
            L = self.max_val
        return L
        
    def get_Kp(self):
        '''
        @brief Prints the encoder's current position
        '''
        return self.Kp
    
    def set_Kp(self, new_val):
        '''
        @brief Sets the encoder's position to a given input
        @param new_val new input value for proportionality constant
        '''
        self.Kp = new_val
        