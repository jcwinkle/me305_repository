'''
@file hw1_main.py
@brief runs two identical elevator tasks at the same time
'''

from hw1_tasks import Button, MotorDriver, TaskElevator
        
## Motor Object
Motor = MotorDriver()

## Button Object for "button 1"
button_1_1 = Button('B_1')

## Button Object for "button 2"
button_2_1 = Button('B_2')

## Button Object for "first floor"
first_1 = Button('Floor_1')

## Button Object for "second floor"
second_1 = Button('Floor_2')

## Button Object for "button 1"
button_1_2 = Button('B_1')

## Button Object for "button 2"
button_2_2 = Button('B_2')

## Button Object for "first floor"
first_2 = Button('Floor_1')

## Button Object for "second floor"
second_2 = Button('Floor_2')

## Task object
task1 = TaskElevator(0.1, Motor, button_1_1, button_2_1, first_1, second_1) # Will also run constructor
task2 = TaskElevator(0.1, Motor, button_1_2, button_2_2, first_2, second_2) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()
