'''
@file lab2_main.py
@brief Runs the VirtualLED and HardwareLED classes defined in lab2_tasks.py
@author Jacob Winkler
'''

from lab2_tasks import TaskVirtualLED, VirtualBlinker, TaskHardwareLED

## Blink Object
Blink = VirtualBlinker()

## Task objects
task1 = TaskVirtualLED(1, Blink) 
task2 = TaskHardwareLED(1)

# To run the task call task1.run() over and over
while True:
    task1.run()
    task2.run()