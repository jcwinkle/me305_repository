'''
@file lab5_Task.py
@brief Takes an input from mobile devive via bluetooth and either turns the LED on, turns the LED off, or flashes it at an input frequency.
@author Jacob Winkler
'''

import pyb
from pyb import UART
import utime


class FreqTask:
    '''
    @brief creates Nucleo Task that calls and blinks an LED at a given frequency
    @details Initializes a pin on the Nucleo that a user can either turn on, turn off, 
    or blink at an input frequency between 1 and 10. The code receives a command
    from an IOS app via bluetooth, and will continue executing that command 
    until another command is input. The possible valid commands that can be 
    received are 'g' (ascii = 103) to turn on the LED, 's' (ascii = 115) to 
    turn off the LED, or any string containing an integer in range 1 to 10, 
    which indicates the frequency at which the LED will blink. This variable 
    frequency operation is done by implementing a miniature FSM within a single
    state of the larger Finite State Machine.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for input value or button push
    S1_WAIT_FOR_INPUT   = 1
    
    ## LED turns on and flashes at input frequency
    S2_LED_ON           = 2
    
    
    def __init__(self, interval):
        '''
        @brief Creates a FreqTask task object.
        @param interval defines the interval at which the task will loop.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## serial initialization class copy
        self.ser = UART(3, 9600)
        
        ## creates class pin object
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
        ## The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
            '''
            @brief Runs one iteration of the task
            '''
            ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
            self.curr_time = utime.ticks_us()
            if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                
                if(self.state == self.S0_INIT):
                    self.pinA5.low()
                    print('LED is initialized')
                    self.transitionTo(self.S1_WAIT_FOR_INPUT)
                    
                
                elif(self.state == self.S1_WAIT_FOR_INPUT):
                    if self.ser.any() != 0:
                        self.cmd = self.ser.read()
                        self.transitionTo(self.S2_LED_ON)
                        
                
                elif(self.state == self.S2_LED_ON):
                    if self.cmd == b'103':
                        print('LED is ON')
                        self.pinA5.high()
                        self.cmd == None
                        self.transitionTo(self.S1_WAIT_FOR_INPUT)
                        
                    elif self.cmd == b'115':
                        print('LED is OFF')
                        self.pinA5.low()
                        self.cmd == None
                        self.transitionTo(self.S1_WAIT_FOR_INPUT)
                        
                    elif int(self.cmd) >= 1 and int(self.cmd) <= 10:
                        nextint = 0
                        ## freq_int converts the input frequency in Hz from 
                        ## self.cmd into a time interval in microseconds.
                        freq_int = int((1/int(self.cmd))*1E6)
                        run_freq = 0
                        print('LED is Blinking at {} Hz'.format(int(self.cmd)))
                        self.cmd == None
                        ## This is a miniature finite state machine within one state...
                        while self.ser.any() == 0:
                            freq_time = utime.ticks_us()
                            if utime.ticks_diff(freq_time, nextint) >= 0:
                                if run_freq%2 == 0:
                                    self.pinA5.high()
                                    nextint = utime.ticks_add(freq_time, freq_int)
                                    run_freq += 1
                                elif run_freq%2 != 0:
                                    self.pinA5.low()
                                    nextint = utime.ticks_add(freq_time, freq_int)
                                    run_freq += 1
                        self.transitionTo(self.S1_WAIT_FOR_INPUT)
                        
                    else:
                        print('Invalid Frequency Input received')
                        self.cmd == None
                        self.pinA5.low()
                        self.transitionTo(self.S1_WAIT_FOR_INPUT) 
                     
                else:
                    # Invalid state code (error handling)
                    pass
                
                self.runs += 1
                
                # Specifying the next time the task will run
                self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState
    
