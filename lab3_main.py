'''
@file lab3_main.py
@brief Main program runs all the tasks for ME 305 lab 3
'''

import lab3_shares
from lab3_EncoderTask import TaskEncoder
from lab3_UserTask import TaskUser
from encoder import Encoder

Encoder = Encoder('A6', 'A7', 3)

## Encoder task encodes and returns desired encoder values
task0 = TaskEncoder(1_000, Encoder)

## User interface task, works with serial port
task1 = TaskUser(1_000)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()