'''
@file lab3_shares.py
@brief A container for all the inter-task variables for ME 305 lab 3
'''

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None