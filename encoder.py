'''
@file encoder.py
@brief Doc defining generic encoder class
@author Jacob Winkler 
'''
import pyb

class Encoder:

    def __init__(self, IN1_pin, IN2_pin, timer):
        '''
        @brief Creates an encoder object.
        @param In1_pin The 1st pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param In2_pin The 2nd pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param timer The timer specific to the physical encoder
        '''
        tim = pyb.Timer(timer)
        tim.init(prescaler=0, period=0xFFFF)
        pin_IN1 = pyb.Pin(IN1_pin)
        pin_IN2 = pyb.Pin(IN2_pin)
        tim.channel(1, pin=pin_IN1, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=pin_IN2, mode=pyb.Timer.ENC_AB)

        self.curr_pos = 0   #sets initial position to 0
        self.prev_pos = 0
        self.tim = tim
        self.offset = 0
            
    def update(self):
        '''
        @brief Updates the encoder's position and records encoder's previous position
        '''   
        self.prev_pos = self.curr_pos
        self.curr_pos = self.tim.counter() - self.offset
        self.delta = self.curr_pos - self.prev_pos
        
    def get_delta(self):
        '''
        @brief Computes the difference between the encoder's current position and previous position
        '''
        if self.delta < (-0xFFFF/2):
            self.good_delta = (0XFFFF-self.prev_pos) +self.curr_pos
        elif self.delta > (0xFFFF/2):
            self.good_delta = -((0xFFFF-self.curr_pos)+self.prev_pos)
        elif self.delta == 0:
            self.good_delta = 0
        else:
            self.good_delta = self.delta
        return int(self.good_delta)
        
    def get_position(self):
        '''
        @brief Prints the encoder's current position
        '''
        return int(self.curr_pos)
    
    def set_position(self, pos):
        '''
        @brief Sets the encoder's position to a given input
        '''
        self.offset = self.curr_pos
        self.curr_pos = pos
        return self.curr_pos
    
    def zero(self):
        '''
        @brief Zeros out the encoder
        '''
        self.offset = self.curr_pos
        self.curr_pos = 0
        return self.curr_pos