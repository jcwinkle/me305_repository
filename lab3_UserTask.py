'''
@file lab3_UserTask.py
@brief User interface task as a finite state machine. Users input one of 3 
       characters: p, d, and z to print the encoder position, print the encoder
       delta, and zero the encoder position respectively. 
'''

import lab3_shares
import utime
from pyb import UART
from encoder import Encoder

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    characters. Users input one of 3 characters: p, d, and z to print the 
    encoder position, print the encoder delta, and zero the encoder position 
    respectively.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_INPUT   = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, interval):
        '''
        Creates a user interface task object.
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('''
                      Please type a command, Your Grace...
                      Type "z" to zero the encoder
                      Type "p" to print the encoder position
                      Type "d" to print the encoder delta
                      ''')
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
            
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    lab3_shares.cmd = self.ser.readchar()
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                if lab3_shares.resp:
                    print('''
                          Another command, Your Grace?
                          Type "z" to zero the encoder
                          Type "p" to print the encoder position
                          Type "d" to print the encoder delta
                          ''')
                    self.transitionTo(self.S1_WAIT_FOR_INPUT)
                    lab3_shares.resp = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState