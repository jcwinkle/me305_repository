'''
@file lab4_Nulceomain.py
@brief Main program imported to nucleo that constantly runs whenever the nucleo
is opened which is referenced by lab4_UserInt.py and calls lab4_EncTask.py. 
Note that this file used to be main.py when lab 4 was run, but a new main.py 
had to be created later for lab 6, so this file was renamed.
@author Jacob Winkler 
'''

from pyb import UART
import utime     
from lab4_EncTask import TaskEncoder
from encoder import Encoder
Encoder = Encoder('A6', 'A7', 3)

task0 = TaskEncoder(Encoder, 2_00000)
while True:
    task0.run()
    '''
    Will Run indefinitely, since this is the main.py file uploded to 
    the Nucleo L-476LG
    '''