'''
@file lab5_main.py
@brief Main program runs all the tasks for ME 305 lab 5
@author Jacob Winkler
'''


from Lab5_Task import FreqTask

task1 = FreqTask(100)
while True:
    task1.run()