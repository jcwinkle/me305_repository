/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Jacob Winkler's ME 305 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "References", "index.html#sec_ref", null ],
    [ "The ME 305 Experience...", "index.html#sec_relatable", null ],
    [ "Lab 1: Fibonacci Calculator", "page_lab1.html", [
      [ "Description", "page_lab1.html#page_lab1_desc", null ],
      [ "Source Code", "page_lab1.html#page_lab1_sourcecode", null ],
      [ "Documentation", "page_lab1.html#page_lab1_doc", null ]
    ] ],
    [ "Homework 1: Finite State Machine Elevator", "page_HW1.html", [
      [ "Description", "page_HW1.html#page_HW1_desc", null ],
      [ "Source Code", "page_HW1.html#page_HW1_sourcecode", null ],
      [ "Documentation", "page_HW1.html#page_HW1_doc", null ]
    ] ],
    [ "Lab 2: Finite State Machine LED Triangle-Wave Flashing", "page_lab2.html", [
      [ "Description", "page_lab2.html#page_lab2_desc", null ],
      [ "Source Code", "page_lab2.html#page_lab2_sourcecode", null ],
      [ "Documentation", "page_lab2.html#page_lab2_doc", null ]
    ] ],
    [ "Lab 3: Incremental Encoder", "page_lab3.html", [
      [ "Description", "page_lab3.html#page_lab3_desc", null ],
      [ "Source Code", "page_lab3.html#page_lab3_sourcecode", null ],
      [ "Documentation", "page_lab3.html#page_lab3_doc", null ]
    ] ],
    [ "Lab 4: Serial Communication with Nucleo, plotting data output", "page_lab4.html", [
      [ "Description", "page_lab4.html#page_lab4_desc", null ],
      [ "Source Code", "page_lab4.html#page_lab4_sourcecode", null ],
      [ "Documentation", "page_lab4.html#page_lab4_doc", null ]
    ] ],
    [ "Lab 5: Bluetooth Communication with Nucleo, Blinking LEDs at input frequency", "page_lab5.html", [
      [ "Description", "page_lab5.html#page_lab5_desc", null ],
      [ "Source Code", "page_lab5.html#page_lab5_sourcecode", null ],
      [ "Documentation", "page_lab5.html#page_lab5_doc", null ]
    ] ],
    [ "Lab 6, Week 1: Motor Driver", "page_lab6.html", [
      [ "Description", "page_lab6.html#page_lab6_desc", null ],
      [ "Source Code", "page_lab6.html#page_lab6_sourcecode", null ],
      [ "Documentation", "page_lab6.html#page_lab6_doc", null ]
    ] ],
    [ "Lab 6, Week 2: Motor Controller", "page_lab6w2.html", [
      [ "Description", "page_lab6w2.html#page_lab6w2_desc", null ],
      [ "Source Code", "page_lab6w2.html#page_lab6w2_sourcecode", null ],
      [ "Documentation", "page_lab6w2.html#page_lab6w2_doc", null ],
      [ "Response Plots", "page_lab6w2.html#page_lab6w2_plots", null ]
    ] ],
    [ "Lab 7: Motor Controller with input profiles", "page_lab7.html", [
      [ "Description", "page_lab7.html#page_lab7_desc", null ],
      [ "Source Code", "page_lab7.html#page_lab7_sourcecode", null ],
      [ "Documentation", "page_lab7.html#page_lab7_doc", null ],
      [ "Response Plots", "page_lab7.html#page_lab7_plots", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"MotorDriver_8py.html",
"hw1__main_8py.html#a8362bb9e1b5b0e495623ec78250fb099"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';