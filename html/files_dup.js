var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop", "classclosedloop_1_1closedloop.html", "classclosedloop_1_1closedloop" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "fibonacci_calculator.py", "fibonacci__calculator_8py.html", "fibonacci__calculator_8py" ],
    [ "hw1_main.py", "hw1__main_8py.html", "hw1__main_8py" ],
    [ "hw1_tasks.py", "hw1__tasks_8py.html", [
      [ "TaskElevator", "classhw1__tasks_1_1TaskElevator.html", "classhw1__tasks_1_1TaskElevator" ],
      [ "Button", "classhw1__tasks_1_1Button.html", "classhw1__tasks_1_1Button" ],
      [ "MotorDriver", "classhw1__tasks_1_1MotorDriver.html", "classhw1__tasks_1_1MotorDriver" ]
    ] ],
    [ "lab2_main.py", "lab2__main_8py.html", "lab2__main_8py" ],
    [ "lab2_tasks.py", "lab2__tasks_8py.html", [
      [ "TaskVirtualLED", "classlab2__tasks_1_1TaskVirtualLED.html", "classlab2__tasks_1_1TaskVirtualLED" ],
      [ "VirtualBlinker", "classlab2__tasks_1_1VirtualBlinker.html", "classlab2__tasks_1_1VirtualBlinker" ],
      [ "TaskHardwareLED", "classlab2__tasks_1_1TaskHardwareLED.html", "classlab2__tasks_1_1TaskHardwareLED" ]
    ] ],
    [ "lab3_EncoderTask.py", "lab3__EncoderTask_8py.html", [
      [ "TaskEncoder", "classlab3__EncoderTask_1_1TaskEncoder.html", "classlab3__EncoderTask_1_1TaskEncoder" ]
    ] ],
    [ "lab3_main.py", "lab3__main_8py.html", "lab3__main_8py" ],
    [ "lab3_shares.py", "lab3__shares_8py.html", "lab3__shares_8py" ],
    [ "lab3_UserTask.py", "lab3__UserTask_8py.html", [
      [ "TaskUser", "classlab3__UserTask_1_1TaskUser.html", "classlab3__UserTask_1_1TaskUser" ]
    ] ],
    [ "lab4_EncTask.py", "lab4__EncTask_8py.html", [
      [ "TaskEncoder", "classlab4__EncTask_1_1TaskEncoder.html", "classlab4__EncTask_1_1TaskEncoder" ]
    ] ],
    [ "lab4_main.py", "lab4__main_8py.html", "lab4__main_8py" ],
    [ "lab4_UserInt.py", "lab4__UserInt_8py.html", "lab4__UserInt_8py" ],
    [ "lab5_main.py", "lab5__main_8py.html", "lab5__main_8py" ],
    [ "lab5_Task.py", "lab5__Task_8py.html", [
      [ "FreqTask", "classlab5__Task_1_1FreqTask.html", "classlab5__Task_1_1FreqTask" ]
    ] ],
    [ "lab6_Nucleomain.py", "lab6__Nucleomain_8py.html", "lab6__Nucleomain_8py" ],
    [ "lab6_PCmain.py", "lab6__PCmain_8py.html", "lab6__PCmain_8py" ],
    [ "lab6_TaskMotor.py", "lab6__TaskMotor_8py.html", [
      [ "MotorTask", "classlab6__TaskMotor_1_1MotorTask.html", "classlab6__TaskMotor_1_1MotorTask" ]
    ] ],
    [ "lab6_UserInt.py", "lab6__UserInt_8py.html", "lab6__UserInt_8py" ],
    [ "lab7_PCmain.py", "lab7__PCmain_8py.html", "lab7__PCmain_8py" ],
    [ "lab7_TaskMotor.py", "lab7__TaskMotor_8py.html", [
      [ "MotorTask", "classlab7__TaskMotor_1_1MotorTask.html", "classlab7__TaskMotor_1_1MotorTask" ]
    ] ],
    [ "lab7_UserInt.py", "lab7__UserInt_8py.html", "lab7__UserInt_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ]
];