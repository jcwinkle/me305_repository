var classlab6__TaskMotor_1_1MotorTask =
[
    [ "__init__", "classlab6__TaskMotor_1_1MotorTask.html#abc99e1d3051e5ca4f90a60e445e86801", null ],
    [ "run", "classlab6__TaskMotor_1_1MotorTask.html#a10118a386282b57ff10359a063269c2b", null ],
    [ "transitionTo", "classlab6__TaskMotor_1_1MotorTask.html#a0e81d59c0d88605192587e994140b409", null ],
    [ "closedloop", "classlab6__TaskMotor_1_1MotorTask.html#aec6d8e9fb3842ccee9718f6fd3ad8989", null ],
    [ "ClosedLoopCont", "classlab6__TaskMotor_1_1MotorTask.html#abb245866466970d69ced7c59d5fed026", null ],
    [ "curr_time", "classlab6__TaskMotor_1_1MotorTask.html#a597f5e25e4c1e27f8c0726f051a60f0c", null ],
    [ "Encoder", "classlab6__TaskMotor_1_1MotorTask.html#a7275a781e15cbb1a03f55b6e08e40dce", null ],
    [ "interval", "classlab6__TaskMotor_1_1MotorTask.html#ae63b6a1b2c18cb7fb6e011b86db4e63d", null ],
    [ "Kp", "classlab6__TaskMotor_1_1MotorTask.html#ae5a8bbcb018ac5fe4198cfb6c680ee97", null ],
    [ "mot_velocity", "classlab6__TaskMotor_1_1MotorTask.html#a90b3376787b52861f99197f5388068af", null ],
    [ "MotorDriver", "classlab6__TaskMotor_1_1MotorTask.html#ace4e16e46759c548ae72f87995d4a10f", null ],
    [ "next_time", "classlab6__TaskMotor_1_1MotorTask.html#a37d3e69677248798b90f42c8b1943b1e", null ],
    [ "ref_vel", "classlab6__TaskMotor_1_1MotorTask.html#a29867d90bb9cd3d7c522d2f1a6e4b772", null ],
    [ "ref_velocity", "classlab6__TaskMotor_1_1MotorTask.html#a16ee4a690e813aa1d2e3991f631858ee", null ],
    [ "ser", "classlab6__TaskMotor_1_1MotorTask.html#a52c1d077718f77ccf3f0aedefe0822b9", null ],
    [ "start_time", "classlab6__TaskMotor_1_1MotorTask.html#a738e50e4b27d64193d4046a3c0d07fad", null ],
    [ "state", "classlab6__TaskMotor_1_1MotorTask.html#afeba1ebbb70c5bcba7dcf5f97059b538", null ],
    [ "time", "classlab6__TaskMotor_1_1MotorTask.html#ad4265e562b72ed909721f42a11f56977", null ]
];