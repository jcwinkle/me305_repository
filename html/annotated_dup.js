var annotated_dup =
[
    [ "closedloop", null, [
      [ "closedloop", "classclosedloop_1_1closedloop.html", "classclosedloop_1_1closedloop" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "hw1_tasks", null, [
      [ "Button", "classhw1__tasks_1_1Button.html", "classhw1__tasks_1_1Button" ],
      [ "MotorDriver", "classhw1__tasks_1_1MotorDriver.html", "classhw1__tasks_1_1MotorDriver" ],
      [ "TaskElevator", "classhw1__tasks_1_1TaskElevator.html", "classhw1__tasks_1_1TaskElevator" ]
    ] ],
    [ "lab2_tasks", null, [
      [ "TaskHardwareLED", "classlab2__tasks_1_1TaskHardwareLED.html", "classlab2__tasks_1_1TaskHardwareLED" ],
      [ "TaskVirtualLED", "classlab2__tasks_1_1TaskVirtualLED.html", "classlab2__tasks_1_1TaskVirtualLED" ],
      [ "VirtualBlinker", "classlab2__tasks_1_1VirtualBlinker.html", "classlab2__tasks_1_1VirtualBlinker" ]
    ] ],
    [ "lab3_EncoderTask", null, [
      [ "TaskEncoder", "classlab3__EncoderTask_1_1TaskEncoder.html", "classlab3__EncoderTask_1_1TaskEncoder" ]
    ] ],
    [ "lab3_UserTask", null, [
      [ "TaskUser", "classlab3__UserTask_1_1TaskUser.html", "classlab3__UserTask_1_1TaskUser" ]
    ] ],
    [ "lab4_EncTask", null, [
      [ "TaskEncoder", "classlab4__EncTask_1_1TaskEncoder.html", "classlab4__EncTask_1_1TaskEncoder" ]
    ] ],
    [ "lab4_UserInt", null, [
      [ "TaskUser", "classlab4__UserInt_1_1TaskUser.html", "classlab4__UserInt_1_1TaskUser" ]
    ] ],
    [ "lab5_Task", null, [
      [ "FreqTask", "classlab5__Task_1_1FreqTask.html", "classlab5__Task_1_1FreqTask" ]
    ] ],
    [ "lab6_TaskMotor", null, [
      [ "MotorTask", "classlab6__TaskMotor_1_1MotorTask.html", "classlab6__TaskMotor_1_1MotorTask" ]
    ] ],
    [ "lab6_UserInt", null, [
      [ "UserInt", "classlab6__UserInt_1_1UserInt.html", "classlab6__UserInt_1_1UserInt" ]
    ] ],
    [ "lab7_TaskMotor", null, [
      [ "MotorTask", "classlab7__TaskMotor_1_1MotorTask.html", "classlab7__TaskMotor_1_1MotorTask" ]
    ] ],
    [ "lab7_UserInt", null, [
      [ "UserInt", "classlab7__UserInt_1_1UserInt.html", "classlab7__UserInt_1_1UserInt" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ]
];