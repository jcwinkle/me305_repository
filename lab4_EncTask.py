'''
@file lab4_EncTask.py
@brief Interacts using a serial interface with lab4_UserTask.py and calls the objects defined in a external encoder class.
@author Jacob Winkler
'''

import utime
from encoder import Encoder
from pyb import UART
import pyb

class TaskEncoder:
    '''
     
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for input state
    S1_WAIT_FOR_INPUT   = 1
    
    
    def __init__(self, Encoder, interval):
        '''
        @brief Creates an encoder task object for ME 305 lab 4 
        @details This task operates on the nucleo and waits for an input from 
        the PC and then records encoder position data for 10 seconds or until 
        the user manually stops it. It then sends the recorded encoder positions 
        and corresponding timestamps as lists back to the PC.
        @param Encoder defines the encoder class which is called by this task
        @param interval defines the interval at which the task will loop
        '''
        
        self.Encoder = Encoder #defines the encoder for this 
        
        self.curr_pos = Encoder.curr_pos #stores class copy of encoder current position
        self.prev_pos = Encoder.prev_pos #stores class copy of encoder previous position
        self.tim = Encoder.tim #stores class copy of encoder timer
        
        self.ser = UART(2)
        
        ## The amount of time in microseconds between runs of the task;
        ## In this case, that is 0.2 seconds, or 200000 microseconds
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.EncPos = []
        self.t = []
        self.t_stamp = []
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        if self.ser.any() != 0:
            self.init = self.ser.readchar()
            if self.init == 103:
                self.init == None
                while True:
                    self.curr_time = utime.ticks_us()
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        
                        self.Encoder.update(self)
                        
                        if(self.state == self.S0_INIT):
                            # Run State 0 Code
                            self.transitionTo(self.S1_WAIT_FOR_INPUT)
                        
                        elif(self.state == self.S1_WAIT_FOR_INPUT):
            

                            if self.ser.readchar() == 115 or utime.ticks_diff(self.curr_time, self.start_time) >= 10000000:
                                for n in range(len(self.t_stamp)):
                                    self.t.append((self.t_stamp[n] - self.t_stamp[0]))
                                #Then begin sending data to PC
                                self.ser.write(str(self.t_stamp) + '  ' + str(self.EncPos) + '  ' + str(self.t))
                                
                            else:
                                Encoder.update(self)
                                self.EncPos.append(self.Encoder.get_position(self))
                                self.t_stamp.append(utime.ticks_diff(self.curr_time, self.start_time))
                     
                        else:
                            # Invalid state code (error handling)
                            pass
                        
                        self.runs += 1
                        
                        # Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
                   
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState