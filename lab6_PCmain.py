'''
@file lab6_PCmain.py
@brief The main file calling the lab 6 User Interface task from lab6_UserInt.py 
on the python kernal.
@author Jacob Winkler
'''

import serial
import time
from matplotlib import pyplot
import numpy as np
from lab6_UserInt import UserInt

task = UserInt(1200, 0.1) # initital ref velocity in RPM, time interval in seconds

## Run Task
while True:
    task.run()