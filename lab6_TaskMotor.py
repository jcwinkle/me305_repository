'''
@file lab6_TaskMotor.py
@brief Task that actuates motor and records response data on the Nucleo. 
@details This file operates on the Nucleo L476 and interacts with lab6_UserInt.py 
on the PC to receive user input Kp and Reference Velocity values, actuate the 
motor according to these inputs, record and format control data, and send the 
relevant, formatted data back to lab6_UserInt.py.
@author Jacob Winkler 
'''

import pyb
from pyb import UART as UART
import utime
from encoder import Encoder
from closedloop import closedloop
from MotorDriver import MotorDriver



class MotorTask:
    '''
    @brief Motor task for ME305 lab 6 that receives user inputs from the PC, actuates 
    the motor, records motor speed data, and sends this data back to the PC
    '''
    
    ## Initializing 
    S0_INIT                 = 0
    
    ## Controlling Data Collection
    S1_CONTROLLING          = 1
    
    ## Ends FSM loop and exits the task; note that this task only runs once 
    S2_FINISH               = 2
    
    def __init__(self, interval):
        '''
        @brief Creates a motor task object
        @param interval defines the interval at which the task will loop.
        '''
        
        ## Initiate Serial Port Communication
        self.ser = UART(2)
        
        ## stores copies of encoder, motordriver, and closedloop classes
        self.Encoder = Encoder('C6', 'C7', 8) 
        self.MotorDriver = MotorDriver('A15', 'B4', 'B5', 3)
        self.closedloop = closedloop
        
        ## updates initial encoder position
        self.Encoder.update()
        
        ## The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enables motor to spin
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        
        ## Preallocate Data Arrays
        self.mot_velocity = []
        self.time = []
        self.ref_velocity = []
        
        ## Initial State
        self.state = self.S0_INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            self.Encoder.update()
            if (self.state == self.S0_INIT):
                if self.ser.any():
                    ## Read Serial input and assign reference velocity
                    self.Kp = float(self.ser.readline())
                    self.ref_vel = int(self.ser.readline())
                    
                    ## Initiate closedloop controller class with the Kp value received from the PC and PWM saturation limits of -100 and 100
                    self.ClosedLoopCont = closedloop(self.Kp, -100, 100)
                    
                    self.transitionTo(self.S1_CONTROLLING)
                    
            if (self.state == self.S1_CONTROLLING):
                if self.ser.any() == 0:
                    ## Read Encoder Value and Convert to RPM
                    PPR = 4000
                    revs = self.Encoder.get_delta()/PPR #converts encoder position delta to number of revolutions in motor shaft
                    int_sec = self.interval*1E-6 #converts interval value to seconds
                    int_min = int_sec/60 #converts interval value to minutes
                    actual_velocity = revs/int_min # calculates motor speed in RPM
                    
                    ## Write relevant data to preallocated arrays
                    self.mot_velocity.append(actual_velocity)
                    self.ref_velocity.append(self.ref_vel)
                    self.time.append((self.curr_time-self.start_time)/1E6)
                    
                    ## Calculate Control Loop Gain
                    L = self.ClosedLoopCont.update(self.ref_vel, actual_velocity)
                    actual_velocity = None
                    
                    ## Change motor duty cycle based on Control Loop Output
                    self.MotorDriver.set_duty(L)
                    
                else:
                    self.MotorDriver.set_duty(0)
                    self.ser.write(str(self.time)+'  '+str(self.mot_velocity)+'  '+str(self.ref_velocity))
                    self.transitionTo(self.S2_FINISH)
                    
            if (self.state == self.S2_FINISH):
                pass
                    
            else:
                pass
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState
            
        