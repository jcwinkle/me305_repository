'''
@file lab7_UserInt.py
@brief User interface task as a finite state machine. 
@details This user interface controls 
a closed-loop p-controller on an Nucleo L476 devboard that controls the speed of 
a DC motor using the provided reference motor velocity and motor position profiles
called from lab7_reference.csv in the Nucleo in lab7_TaskMotor.py. It sends a 
Kp Gain value to the Nucleo, then plots the resulting motor speed response.
@author Jacob Winkler
'''

import time
import serial
import matplotlib.pyplot as plt
import array
import csv

# initialize serial port for PC to Nucleo communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


class UserInt:
    '''
    @brief User interface task for ME305 lab 7 that sends the input Kp value to the 
    Nucleo, then receives and plots the reference velocity and position profiles 
    alongside the actual Motor Velocity and Position Response data.
    '''
    
    ## Initialization state: setting Kp
    S0_INIT             = 0
    
    ## Collecting and plotting data
    S1_DATA             = 1
    
    ## Finishes FSM loop and exits task; note that this task only runs once
    S2_FINISH           = 2
    
    def __init__(self, interval):
        '''
        @brief Creates a user interface task object.
        @param interval defines the interval at which the task will loop.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## stores a class copy of imports
        self.ser = ser
        self.array = array 
        self.csv = csv
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''  
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                Kp_init = input('Specify a Kp input, Human. Kp = ')
                self.Kp = int(float(Kp_init)*1E6)/1E6
                if self.Kp <= 0:
                    In = input('Please input a positive, nonzero Kp value: ')
                    self.Kp = int(float(In)*1E6)/1E6 
                elif self.Kp > 0:
                    Kp = str(self.Kp)+'\n'
                    
                    # writes input controller parameters
                    self.ser.write(Kp.encode())

                    print('''
                          Hello Human! We are interfacing with a Closed Loop 
                          P-controller to control the speed of the motor using your 
                          Nucleo L476 devboard. Running the controller with Kp = {}. 
                          '''.format(self.Kp))
                    
                    self.transitionTo(self.S1_DATA)
                
            elif(self.state == self.S1_DATA):
                if self.ser.in_waiting != 0:
                
                    # Initiate Reading Data
                    J = ser.readline().decode('ascii')
                    time_stamp = ser.readline().decode('ascii').strip("array('f', ")
                    vel_act = ser.readline().decode('ascii').strip("array('f', ")
                    pos_act = ser.readline().decode('ascii').strip("array('f', ")
                    vel_ref = ser.readline().decode('ascii').strip("array('f', ")
                    pos_ref = ser.readline().decode('ascii').strip("array('f', ")
                    
                    #the self.time array is based off of when the Nucleo is 
                    #first initialized, rather than when the motor starts 
                    #spinning. Thus, must subtract each element by timestamp[0] to 
                    #set t=0 to when the motor starts spinning
                    time_act = [t - time_stamp[0] for t in time_stamp]
                    
                    ## Plotting Data
                    # Velocity
                    fig, axes = plt.subplots(2)
                    fig.suptitle('Motor Responses and Reference Profiles with Kp = '+str(self.Kp)+' and J = '+str(J))
                    plt.subplot(2, 1, 1)
                    plt.plot(time_act, vel_act, 'b', time_act, vel_ref, 'r--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Motor Velocity (RPM)')
                    
                    # Position
                    plt.subplot(2, 1, 2)
                    plt.plot(time_act, pos_act, 'b', time_act, pos_ref, 'r--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Motor Position (Degrees)')
                    
                    self.transitionTo(self.S2_FINISH)
            
            elif(self.state == self.S2_FINISH):
                quit()
                        
            self.next_time += self.interval

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState