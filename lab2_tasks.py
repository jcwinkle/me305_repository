'''
@file lab2_tasks.py
@brief defines all of the classes and tasks for lab 2. Note that this lab runs entirely on the Nucleo through PuTTY
@details This file defines 3 classes: 
VirtualBinker, a class that "blinks" a virtual LED by printing "LED On and 
"LED Off", TaskVirtualLED, a task that calls the VirtualBlinker class and uses 
a finite state machine to blink a virtual LED, and TaskHardwareLED, a task that
calls the nucleo pin corresonding to a given LED, and blinks that LED in an 
increasing triangle-wave pattern at a given interval.
@author Jacob Winkler
'''

import utime
import pyb

class TaskVirtualLED:
    '''
    @brief      A finite state machine to control a Virtual LED
    @details    This class implements a finite state machine to control a virtual LED
                and make it blink on and off every 1.0 seconds.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1 - Turns Vurtual LED off
    S1_LED_OFF              = 1
    
    ## Constant defining State 2 - Turns Virtual LED on
    S2_LED_ON               = 2
       
    def __init__(self, interval, Blink):
        '''
        @brief Creates a TaskVirtualLED object that turns a Virtual LED on and off at a given interval.
        @param interval defines the interval at which the task will loop.
        @param Blink defines the class that will be used to blink the virtual LED
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
    
        ## A class attribute "copy" of the VirtualBlinker object called in lab2_main.py
        self.Blink = Blink # Stores class copy of Blink so other functions can
                           # use the Blink object
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The mircosecond timestamp of the task starting
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()   #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print('The Virtual LED has been turned on, and will start blinking shortly!')
                # Run State 0 Code
                self.transitionTo(self.S2_LED_ON)
            
            elif(self.state == self.S1_LED_OFF):
                self.transitionTo(self.S2_LED_ON)
                self.Blink.Off()
            
            elif(self.state == self.S2_LED_ON):
                self.transitionTo(self.S1_LED_OFF)
                self.Blink.On()
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState The new state variable for the FSM
        '''
        self.state = newState
        
        
class VirtualBlinker:
    '''
    @brief      A Virtual Blinking LED
    @details    This class represents a Virtual Blinking LED. It 
                turns on and off every 1.0 seconds.
    '''
    
    def __init__(self):
        '''
        @brief Creates a VirtualBlinker Object
        '''
        pass
    
    def On(self):
        '''
        @brief Turns on virtual LED
        '''
        print('Task 1: Virtual LED ON!')
    
    def Off(self):
        '''
        @brief Turns off virtual LED
        '''
        print('Task 1: Virtual LED OFF!')
    
    def Stop(self):
        '''
        @brief Stops the virtual LED
        '''
        print('Task 1: LED has stopped blinking!')


class TaskHardwareLED:
    '''
    @brief      A finite state machine to control a real-life LED on a Nucleo
    @details    This class controls LED2 on a Nucleo L476RG in real life. The 
                code blinks the LED in an increasing triangle wave pattern with 
                a period of 10 seconds. The code increases the brightness of 
                the LED by 10 percent every 1 second and runs indefinitely.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_LED_OFF              = 1
    
    ## Constant defining State 2
    S2_LED_brightening      = 2
       
    def __init__(self, interval):
        '''
        @brief Creates a TaskHardareLED object.
        @param interval defines the interval at which the task will loop.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
    
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The mircosecond timestamp of the task starting
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets output to LED 2 on Nucleo
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
    
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()   #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print('The LED is primed and ready! It will start its blinking pattern shortly!')
                # Run State 0 Code
                self.transitionTo(self.S2_LED_brightening)
            
            elif(self.state == self.S1_LED_OFF):
                self.pinA5.low ()
                print('Task 2: LED at 0% Brightness!')
                self.transitionTo(self.S2_LED_brightening)
            
            elif(self.state == self.S2_LED_brightening):
                ## The variable LED_percent_brightness increments the LED 
                ## brightness to increase by 10% for each given time interval.
                ## It does this by taking the remainder of the difference 
                ## between the current time of each iteration and the initial 
                ## time the code started running, in microseconds, from the 
                ## desired period of the triangle wave, which is defined to be 
                ## 10 seconds, or 1e7 microseconds. This remainder is then 
                ## multiplied by 1e-5 to reduce it to a properly scaled 
                ## percentage value that works in the 'pulse_width_percent' 
                ## function. This value is then rounded to get a whole integer.
                LED_percent_brightness = round(1e-5*(utime.ticks_diff(self.curr_time, self.start_time)%1e7))
                if (LED_percent_brightness < 100):
                    self.t2ch1.pulse_width_percent(LED_percent_brightness)
                    print('Task 2: LED at {}% Brightness!'.format(LED_percent_brightness))
                elif (LED_percent_brightness == 100):
                    print('Task 2: LED at {}% Brightness!'.format(LED_percent_brightness))
                    self.transitionTo(self.S1_LED_OFF)
                else:
                    pass
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        @param newState The new state variable for the FSM
        '''
        self.state = newState
        
