'''
@file lab6_Nucleomain.py
@brief Main program imported to nucleo that runs whenever the nucleo
is opened and calls lab6_TaskMotor.py 
'''

import pyb
from pyb import UART as UART
import utime
from encoder import Encoder
from MotorDriver import MotorDriver
from closedLoop import closedloop
from lab6_TaskMotor import MotorTask

## interval is 20 milliseconds (20000 microseconds) 
task = MotorTask(2_0000) 

while True:
    task.run()
    '''
    Will Run indefinitely, since this is the main.py file uploded to 
    the Nucleo L-476LG
    '''
