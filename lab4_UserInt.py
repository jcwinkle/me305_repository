'''
@file lab4_UserInt.py
@brief lab 4 User interface task as a finite state machine.
@author Jacob Winkler 
'''


import time
import csv
import numpy as np
import serial
import matplotlib.pyplot as plt

# initialize serial port for PC to Nucleo communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)


class TaskUser:
    '''
    @brief User interface task.
    @details An object of this class interacts with serial port UART2 waiting for 
    characters. Users input "g" to start collecting data from an encoder 
    connected to a nucleo L-476RG. This then triggers the nucleo to begin 
    recording encoder position every 0.2 seconds for 10 seconds. The user can 
    end the data collection early by pressing "s". After data collection ends, 
    this code generates a plot of encoder position vs. time and outputs a .CSV 
    data file containing encoder positions and corresponding timestamps.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for input state
    S1_WAIT_FOR_INPUT   = 1
    
    ## collecting & recording data state
    S2_COLLECT_ENC_DATA = 2
    
    ## plotting data state
    S3_PLOT_ENC_DATA    = 3
    
    def __init__(self, interval):
        '''
        @brief Creates a user interface task object.
        @param interval defines the interval at which the task will loop.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## stores a class copy of the serial initialization
        self.ser = ser
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        

    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''  
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                    # Run State 0 Code
                print('''
                      You may start recording encoder data at your leisure, Your Grace...
                      ''')
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
                
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                # Run State 1 Code
                self.In = input(' Type "g" to start recording data: ')
                
                if self.In == 'g':
                    ser.write(str(self.In).encode('ascii'))
                    #Clear Command
                    self.In = None
                    self.transitionTo(self.S2_COLLECT_ENC_DATA)
                    datatime = time.time()
                else:
                    self.In = input('That is not a valid input, Your Grace. Please Try Again... ') 
                   
            elif(self.state == self.S2_COLLECT_ENC_DATA):
                self.term = input('To terminate, press s: ')
                #Handle Early Termination
                if self.term == 's' or time.time-datatime >= 10:
                    if self.term == 's':
                        #Send input
                        self.ser.write(str(self.term).encode('ascii'))
                        #Clear Command
                        self.term = None
                        
                    #Initiate data reading
                    data = list(ser.readline().decode('ascii').split('  '))
                    out = [m.strip('[]').split(', ') for m in data]
                    self.time_stamp = [int(t) for t in out[0]]
                    self.enc_pos = [int(t)/3.89 for t in out[1]]
                    # for the motor/encoder used, the conversion is 1 degree = 3.89 ticks
                    self.time_list = [int(t)/1E6 for t in out[2]]
                    self.transitionTo(self.S3_PLOT_ENC_DATA)
                elif(self.term != 's'):
                    self.term = input('Please enter a valid command(Data collection intialization is unavaible during collection): ')
                else:
                    pass
                
            elif(self.state == self.S3_PLOT_ENC_DATA):   
                #Plotting Data
                plt.plot(self.time_list, self.enc_pos)
                plt.xlabel('Time (s)')
                plt.ylabel('Encoder Position (degreees)')
                plt.title('Encoder Position vs. Time:')
                filename = 'Encoder Output'+time.strftime('%Y%m%d-%H%M%S')
                
                #Creating CSV File
                np.savetxt(filename, [self.time_stamp, self.enc_pos], delimiter = ', ')
                self.transitionTo(self.S1_WAIT_FOR_INPUT)    
                        
            self.next_time += self.interval

            
        else:
            # Invalid state code (error handling)
            pass
            
        self.runs += 1

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState