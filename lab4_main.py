'''
@file lab4_main.py
@brief Main program runs all the tasks for ME 305 lab 4 on the PC
@author Jacob Winkler
'''

import serial
import time
from matplotlib import pyplot
import csv
from lab4_UserInt import TaskUser

task1 = TaskUser(1)
while True:
    task1.run()
    '''
    Will Run indefinitely, since this is interacts with the main.py file uploded to 
    the Nucleo L-476LG
    '''