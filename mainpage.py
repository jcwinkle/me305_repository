## @file mainpage.py
##
## @mainpage
##
## @section sec_intro Introduction
## This site contains the Lab projects created by Jacob Winkler for the class 
## ME 305, Introduction to Mechatronics. Jacob created this site as a portfolio 
## to showcase his work throughout the class. Jacob also likes talking about 
## himself in the third person, as he is doing here. Although he admits this is 
## a bit weird, he hopes you will at least find his work on this site interesting 
## and/or useful! The Lab assignments and other work are listed to the left in 
## the treeview. \n
## \n
## To return back to Jacob's Portfolio index, click here: \n
## https://jcwinkle.bitbucket.io/index.html
##
## @section sec_ref References
## Jacob fully admits his mortal self would not have been able to
## create all of this code and documentation without assistance. Throughout the
## course of ME 305, Jacob worked with his fellow engineers, Anil Singh and Kai Quizon, 
## and referenced thier code when developing his own. Their code and documentation
## can be found here: \n
## \n
## Anil Singh's Website: https://asinghcp.bitbucket.io \n
## Kai Quizon's Website: https://kquizon.bitbucket.io \n
## \n
## Additionally, Jacob would like to thank Charlie Refvem and Dr. 
## Eric Espinoza-Wade for their teachings and guidance in writing this code and
## generating this documentation. It would have been impossible without them.
##
## @section sec_relatable The ME 305 Experience...
## Below are some images that truly encapsulate Jacob's time working on ME 305 assignments:
##
## @image html l.jpg width=400px \n
## \n
## @image html m.jpg width=400px \n
## \n
## @image html n.jpg width=400px \n
## \n
## @image html o.jpg width=400px \n
## \n
## @image html p.jpg width=400px \n
## \n
## @author Jacob Winkler
##
## @date This page was created on September 28, 2020
##
## @page page_lab1 Lab 1: Fibonacci Calculator
## @section page_lab1_desc Description
## fibonacci_calculator.py prompts the user to specify a positive integer
## index, calculates the fibonacci number at that index, asks the user 
## if they want to calculate another index, and repeats if the user answers 
## "Yes". To do this, a while loop is used, stipulating that while string_in 
## is "Yes", the function fib will run, calculating the fibonacci number 
## at the desired index input by the user. After fib runs and the 
## desired fibonacci number is output, the fibonacci calculator.py 
## program asks if the user would like to calculate another fibonacci 
## number. The user's input redefines string_in. If the user inputs 
## "Yes", the program will repeat. If the user inputs anything other than 
## "Yes", he program will stop running. It then uses if/else statements 
## to print the proper grammar for the index (ie. 1st, 2nd, 3rd, etc.) 
## along with the desired fibonacci number. 
##
## @section page_lab1_sourcecode Source Code
## Here is the link to the source code: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/fibonacci_calculator.py
##
## @section page_lab1_doc Documentation
## Here is the documentation link for the Fibonacci Calculator: \ref fibonacci_calculator.py
##
## @page page_HW1 Homework 1: Finite State Machine Elevator
## @section page_HW1_desc Description
## This file serves as an example implementation of a finite-state-machine using
## Python. The example will implement some code to control an imaginary elevator 
## operating between two floors.
##
## The user has a button to go to floor 1, and another button to go to floor 2.
##
## The elevator automatically stops once it reaches a floor and waits for another 
## user to come and press a button to take them to another floor
##
## Below is a Hand-drawn State Transition Diagram used in designing the code: \n
##
## @image html HW1STDiagram.PNG width=500px
##
## @section page_HW1_sourcecode Source Code
## Here is a link to the script containing the elevator tasks: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/hw1_tasks.py
##
## Here is a link to the main script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/hw1_main.py
##
## @section page_HW1_doc Documentation
## Here is the documentation link for the elevator tasks file: \ref hw1_tasks.py
##
## @page page_lab2 Lab 2: Finite State Machine LED Triangle-Wave Flashing
## @section page_lab2_desc Description
## This file serves as an example of how to program on real hardware through an
## REPL! It uses a Finite State Machine to both blink a virtual LED and operate
## an actual LED on a Nucleo L476RG. The code is designed to change states every
## 1.0 seconds and to run indefinitely until interupted. The two main tasks 
## are described below:
##
## Task1 implements a finite state machine to control a virtual LED
## and make it blink on and off every 1.0 seconds, running indefintely. 
##
## Task2 controls LED2 on a Nucleo L476RG in real life. The 
## code blinks the LED in an increasing triangle wave pattern with 
## a period of 10 seconds. The code increases the brightness of 
## the LED by 10 percent every 1 second and runs indefinitely.
##
## Below is a Hand-drawn State Transition Diagram used in designing the code: \n
##
## @image html Lab2ST.JPG width=700px
##
## @section page_lab2_sourcecode Source Code
## Here is a link to the detailed source code: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab2_tasks.py
##
## Here is a link to the main script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab2_main.py
##
## @section page_lab2_doc Documentation
## Here is the documentation link for the tasks that blink the virtual and real 
## LEDs: \ref lab2_tasks.py
##
## @page page_lab3 Lab 3: Incremental Encoder
## @section page_lab3_desc Description
## This lab made use of serial communication in order for two tasks to run on 
## differnt .py files interdependently. Conjointly, the files listed below operate
## a simple user interface with a non-rotating motor attached to a Nucleo L476RG.
## The user inputs one of three basic one-character commands to give or receive 
## information about the encoder. The commands are: p, d, and z to print the 
## encoder position, print the encoder delta, and zero the encoder position 
## respectively.
##
## Below is an image of the hand-drawn State Transition diagrams 
## for the two tasks: \n
## 
## @image html Lab3_State_Trans.JPG width=800px
##
## @section page_lab3_sourcecode Source Code
## Here is a link to the main script that defines the encoder timer and runs 
## the tasks simultaneously: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab3_main.py
##
## Here is a link to the script containing the encoder class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/encoder.py
##
## Here is a link to the User Interface task that prompts the user for inputs 
## and relays those commands to the Encoder task: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab3_UserTask.py
##
## Here is a link to the Encoder task that receives commands from the User task
## and calls objects from the encoder class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab3_EncoderTask.py
##
## Here is a link to the file with the shared variables: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab3_shares.py
##
## @section page_lab3_doc Documentation
## Here are the documentation links for the Encoder class, UI task, and Encoder
## task: \n
## \ref encoder.py \n
## \ref lab3_UserTask.py \n
## \ref lab3_EncoderTask.py \n
##
## @page page_lab4 Lab 4: Serial Communication with Nucleo, plotting data output
## @section page_lab4_desc Description
## This lab made use of serial communication between the main file that runs 
## upon startup on the Nucleo and a user interface running on a connected computer.
## The goal of this communication is to record positions on the encoder for a 
## set period of time, plot the position vs. time in python running locally on 
## the computer, and create a .CSV file containing the encoder positions and 
## corresponding timestamps.
##
## On the User Interface, Users input "G" to start collecting data from an encoder 
## connected to a nucleo L-476RG. This then triggers the nucleo to begin 
## recording encoder position every 0.2 seconds for 10 seconds. The user can 
## end the data collection early by pressing "S". After data collection ends, 
## this code generates a plot of encoder position vs. time and outputs a .CSV 
## data file containing encoder positions and corresponding timestamps.
##
## Below is an image of the hand-drawn State Transition diagrams 
## for the two tasks: \n
## 
## @image html Lab4_ST.JPG width=800px
##
## @section page_lab4_sourcecode Source Code
## Here is a link to the main script that runs upon startup on the Nucleo: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab4_Nucleomain.py
##
## Here is a link to the Lab4_EncTask.py script called in the main script above. 
## This script contains the meat of the code: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab4_EncTask.py
##
## Here is a link to the User Interface task that prompts the user for inputs, 
## relays those commands to the Nucleo task, and receives/plots the resulting 
## data from the Nucleo: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab4_UserInt.py
##
## Here is a link to the Lab4_main.py script that runs the UI described above: \n 
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab4_main.py
##
## @section page_lab4_doc Documentation
## Here are the documentation links for the UI and Encoder Task files: \n
## \ref lab4_UserInt.py \n
## \ref lab4_EncTask.py \n 
##
## @page page_lab5 Lab 5: Bluetooth Communication with Nucleo, Blinking LEDs at input frequency
## @section page_lab5_desc Description
## This lab contains a task that runs on the nucleo that waits for input from an iphone app.
## Using a Bluetooth connection, the Nucleo receives commands that will either 
## turn an LED on, turn it off, or blink it at an input frequency between 1 and 10 Hertz.
## The code will continue executing this command until another command is input. 
## The possible valid commands are 'g' (ascii = 103) to turn on the LED, 's' (ascii = 115)
## to turn off the LED, or any string containing an integer in range 1 to 10, 
## which indicates the frequency at which the LED will blink. This variable 
## frequency operation is done by implementing a miniature FSM within a single
## state of the larger Finite State Machine.
## 
## The code will automatically change the LED's frequency if the user submits a 
## new frequency value, even if it is currently flashing at a predetermined frequency. 
## Additionally, if one of the valid input commands is not entered, then the code 
## will turn off the LED and wait for a valid input command. 
##
## Below is an image of the hand-drawn State Transition diagram and Task Diagram: \n
## 
## @image html Lab5_image.JPG width=800px
##
## @section page_lab5_sourcecode Source Code 
## Here is a link to the Lab5_main.py that runs the FSM: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab5_main.py
##
## Here is a link to the LED blinking task that contains the FSM: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab5_Task.py
##
## Here is the link to the Thunkable  code for the IOS bluetooth app: \n
## https://x.thunkable.com/projects/5fa18a0b52851d441a46d09d/d602917c-a412-41fd-95c9-2ff5c2700f34/blocks
##
## @section page_lab5_doc Documentation
## Here is the documentation link for the LED blinking task file: \ref lab5_Task.py  
##
## @page page_lab6 Lab 6, Week 1: Motor Driver
## @section page_lab6_desc Description
## This lab contains the script file containing a MotorDriver class and some test 
## code. The class can enable the motor to spin and disable it. Additionally, the 
## set_duty function in the class can set the speed and direction of the motor 
## using pulse width modulation. Entering a positive percent value, x, between 0 and 100 
## into the set_duty function causes the motor to spin forward at x% of its max speed, 
## and entering a negative percent value, y, causes it to spin backward at y% of its max speed.
## A value of zero makes vthe motor stop spinning.
##
## @section page_lab6_sourcecode Source Code
## Here is a link to the script containing the Motor Driver class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/MotorDriver.py
##
## @section page_lab6_doc Documentation
## Here is the documentation link for the Motor Driver file: \ref MotorDriver.py 
##
## @page page_lab6w2 Lab 6, Week 2: Motor Controller
## @section page_lab6w2_desc Description
## On a large scale, the various scripts below all interact with each other to 
## create a closed-loop p-controller on an Nucleo L476 devboard that controls 
## the speed of a DC motor using a predetermined reference velocity as a step 
## input. The reference velocity value is set in the lab6_PCmain script, 
## which calls lab6_UserInt, where the user inputs Kp. Then, the lab6_UserInt 
## sends the controller values to the Nucleo. There, the lab6_TaskMotor script 
## acts as a closed loop p-controller for a pre-specified motor on the devboard, 
## controlling the motor velocity response to the specified input reference 
## velocity using the closedloop, MotorDriver, and encoder classes. The motor 
## eventually reaches a steady state velocity, 
## and an encoder records an array representing the actual motor velocity 
## response and sends it back to lab6_UserInt, where it is plotted with respect
## to time. It should be noted that the motor will begin to spin when the 
## PCmain is run, and will continue to spin until the user puts in the command "s" 
## to stop the data collection. It is critical that the User does not let the 
## Nucleo run too long, because it only has limited memroy on which to store 
## data These scripts run only once and produce one graph, and then the program quits. 
##
## Below are Hand-drawn State Transition and Task Diagrams used in designing the code: \n
##
## @image html Lab6ST.JPG width=700px
## 
## @section page_lab6w2_sourcecode Source Code
## 
## Here is a link to the lab6_PCmain script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab6_PCmain.py
##
## Here is a link to the lab6_UserInt script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab6_UserInt.py
##
## Here is a link to the lab6_TaskMotor script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab6_TaskMotor.py
##
## Here is a link to the main script calling the Tasks on the Nucleo: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab6_Nucleomain.py
##
## Here is a link to the script containing the MotorDriver class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/MotorDriver.py
##
## Here is a link to the script containing the Encoder class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/encoder.py
##
## Here is a link to the script containing the closedloop class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/closedloop.py
##
## @section page_lab6w2_doc Documentation
## Here are the documentation links for the UI and Motor Driver Task files: \n
## \ref lab6_UserInt.py \n
## \ref lab6_TaskMotor.py \n
## \n
## Here are the documentation links for the encoder, MotorDriver, and closedloop
## class files: \n
## \ref encoder.py \n
## \ref MotorDriver.py \n
## \ref closedloop.py \n
##
## @section page_lab6w2_plots Response Plots
## Motor Speed Response Plots for various Kp values are shown below for a 
## reference velocity of 1200 RPM.  The steady state error of the response 
## decreases as Kp increases. However, so does the percent overshoot, and 
## eventually the system becomes unstable around Kp = 0.25. After much 
## experimental tuning, it was determined that a value of Kp between 0.08 and 
## 0.12 produces the best motor speed response at 1200 RPM.
## 
## @image html lab6_Kp005.png width=500px
##
## @image html lab6_Kp008.png width=500px
##
## @image html lab6_Kp01.png width=500px
##
## @image html lab6_Kp012.png width=500px
##
## @image html lab6_Kp015.png width=500px
##
## @image html lab6_Kp02.png width=500px
##
## @image html lab6_Kp025.png width=500px
##
## @page page_lab7 Lab 7: Motor Controller with input profiles
## @section page_lab7_desc Description
## This lab is merely an adaptation of the lab 6, week 2 code. The Kp value is 
## set in lab7_UserInt, which sends this value to the Nucleo. 
## There, the lab7_TaskMotor script generates Velocity and Position profiles 
## and acts as a closed loop p-controller for a pre-specified motor on the devboard.  
## It then controls the motor velocity and position responses according to 
## to the generated profiles using the closedloop, MotorDriver, 
## and encoder classes. lab7_TaskMotor also records arrays representing the 
## actual velocity and position responses and sends all the data arrays 
## (reference velocity, reference position, actual velocity, actual position, 
## and time) back to lab6_UserInt, where it is plotted with respect
## to time. It should be noted that the motor should spin for 15 seconds 
## and then the data collection will stop. These scripts run only once and produce one 
## graph, and then the program quits.
##
## Below are Hand-drawn State Transition and Task Diagrams used in designing the code: \n
##
## @image html Lab7ST.JPG width=700px 
##
## As of now, This code does not run. There is an issue with timing on the Nucleo
## side, that neither me nor my aforementioned peers, Anil Singh and Kai Quizon,
## were able to solve. Additionally, my hardware will not spin at the low RPM 
## values in the velocity profile generated from lab7_reference.csv. However, 
## the code linked and documented below gives the general structure of how to 
## run the motors according to input position and velocity profiles.
## 
## @section page_lab7_sourcecode Source Code
## 
## Here is a link to the lab7_PCmain script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab7_PCmain.py
##
## Here is a link to the lab7_UserInt script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab7_UserInt.py
##
## Here is a link to the lab7_TaskMotor script: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab7_TaskMotor.py
##
## Here is a link to the main script calling the Tasks on the Nucleo: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/main.py
##
## Here is a link to the script containing the MotorDriver class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/MotorDriver.py
##
## Here is a link to the script containing the Encoder class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/encoder.py
##
## Here is a link to the script containing the closedloop class: \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/closedloop.py
##
## Here is a link to the csv file that generates the reference profiles on the Nucleo: \n
## Column 1 is time, column 2 is velocity in RPM, and column 3 is position in degrees. \n
## https://bitbucket.org/jcwinkle/me305_repository/src/master/lab7_reference.csv
## 
## @section page_lab7_doc Documentation
## Here are the documentation links for the UI and Motor Driver Task files: \n
## \ref lab7_UserInt.py \n
## \ref lab7_TaskMotor.py \n
## \n
## Here are the documentation links for the encoder, MotorDriver, and closedloop
## class files: \n
## \ref encoder.py \n
## \ref MotorDriver.py \n
## \ref closedloop.py \n
##
## @section page_lab7_plots Response Plots
## Since our code failed to run and generate graphs, Kai Quizon simulated what 
## the motor Response should approximately look like alongside the reference profiles.\n
## Here is a link to his page containing this simulated graph and how it was generated: \n
## https://kquizon.bitbucket.io/page_velprofile.html