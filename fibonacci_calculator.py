'''
@file       fibonacci_calculator.py
@brief      fibonacci_calculator.py is a program that calculates the user's desired fibonacci number.
@details    fibonacci_calculator.py prompts the user to specify a positive integer
            index, calculates the fibonacci number at that index, asks the user 
            if they want to calculate another index, and repeats if the user answers 
            "Yes". To do this, a while loop is used, stipulating that while string_in 
            is "Yes", the function fib will run, calculating the fibonacci number 
            at the desired index input by the user. After fib runs and the 
            desired fibonacci number is output, the fibonacci calculator.py 
            program asks if the user would like to calculate another fibonacci 
            number. The user's input redefines string_in. If the user inputs 
            "Yes", the program will repeat. If the user inputs anything other than 
            "Yes", he program will stop running. It then uses if/else statements 
            to print the proper grammar for the index (ie. 1st, 2nd, 3rd, etc.) 
            along with the desired fibonacci number.
@author     Jacob Winkler
@date       September 27, 2020
'''
##@brief        string_in determines whether to run fibonacci calculator or not.
##@details      string_in starts off as "Yes" because by running the program, 
##              it is assumed that the user wants to calculate at least one fibonacci
##              number. A while loop is then used, stipulating that while string_in 
##              is "Yes", the function fib will run. After fib runs and the 
##              desired fibonacci number is output, the fibonacci calculator.py 
##              program asks if the user would like to calculate another fibonacci 
##              number. The user's input redefines string_in. 
string_in = "Yes"
while string_in == "Yes":
    ##@brief    string_idx is the desired fibonacci number, input by the user as a string.
    string_idx = input('Please Enter Desired Fibonacci Index: ')
    
    ##@brief    idx is the desired fibonacci number input by the user converted to an integer.
    idx = int(string_idx)
    
    '''
    @brief      fib calculates the fibonacci number based on the given index.
    @details    fib defines the first two indices of the fibonacci series as f_a = 0
                and f_b = 1. It uses if/else statements to return 0 for the 0th 
                index, 1 for the 1st index, and an error for any negative index 
                that is input by the user. For indices greater than or equal to 2,
                it uses a while loop to add f_a and f_b (creating their sum, f_n), 
                redefine f_a as the old value of f_b, redefine f_b to be equal 
                to f_n, and loop this process until it reaches the fibonacci number
                of the desired index. 
    '''
    def fib (idx):
        f_a = 0
        f_b = 1
        if idx < 0:
            print('Fibonacci sequence is only valid for positive indices. Please' 
             ' input a positive index!')
        elif idx == 0:
            return f_a
        elif idx == 1:
            return f_b
        elif idx >= 2:
            i = 2
            while (i <= idx):
                f_n = f_a + f_b
                f_a = f_b
                f_b = f_n
                i = i + 1
            return f_n
    if idx == 1:
        print('The 1st Fibonacci Number is n = ', fib(idx))
    elif idx == 2:
        print('The 2nd Fibonacci Number is n = ', fib(idx))
    elif idx == 3:
        print('The 3rd Fibonacci Number is n = ', fib(idx))
    else:
        print('The {}th Fibonacci Number is n = '.format(idx), fib(idx))
    string_in = input("Would you like to keep calculating Fibonacci numbers? ")
    if string_in != "Yes":
        print("Okay, Goodbye...")
        