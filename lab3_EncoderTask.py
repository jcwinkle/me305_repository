'''
@file lab3_EncoderTask.py
@brief Interacts using a serial interface with lab3_UserTask.py and calls the 
       objects defined in a external encoder class.
'''

import lab3_shares
import utime
from encoder import Encoder

class TaskEncoder:
    '''
    @brief 
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1

    def __init__(self, interval, Encoder):
        '''
        Creates an encoder task object.
        '''
        
        self.Encoder = Encoder #stores class copy of encoder
        
        self.curr_pos = Encoder.curr_pos #stores class copy of encoder current position
        self.prev_pos = Encoder.prev_pos #stores class copy of encoder previous position
        self.tim = Encoder.tim #stores class copy of encoder timer
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            Encoder.update(self)
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                if lab3_shares.cmd:
                    # Note: in the ASCII cypher, z = 122, p = 112, d = 100
                    if lab3_shares.cmd == 122:
                        Encoder.zero(self)
                    elif lab3_shares.cmd == 112:
                        Encoder.get_position(self)
                    elif lab3_shares.cmd == 100:
                        Encoder.get_delta(self)
                    else:
                        print('That is not a valid command, Your Grace. Please Try again.')
                    lab3_shares.resp = lab3_shares.cmd
                    lab3_shares.cmd = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
