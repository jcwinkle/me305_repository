'''
@file lab7_PCmain.py
@brief The main file calling the lab 7 User Interface task from lab7_UserInt.py 
on the python kernal.
@author Jacob Winkler
'''

import serial
import time
from matplotlib import pyplot
import numpy as np
from lab7_UserInt import UserInt

task = UserInt(0.1) # time interval is 0.1 seconds

## Run Task
while True:
    task.run()
